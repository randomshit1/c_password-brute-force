#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>

bool extracted = false;

bool extract_archive(const char *password, const char *input, const char *output)
{
    int size = snprintf(NULL, 0, "7z x -p'%s' -o%s -aoa %s 1>NUL 2>NUL", password, output, input) + 1;

    char *command = (char *)malloc(size);
    if (command == NULL)
    {
        return false;
    }

    snprintf(command, size, "7z x -p'%s' -o%s -aoa %s 1>NUL 2>NUL", password, output, input);

    int result = system(command);
    free(command);

    return result == 0 ? true : false;
}
int main()
{
    const char *ValidChars = " ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()_+-={}[]|\\:;\"'<>,.?/~`";
    const int MinimLen = 1;
    const int MaximLen = 130;
    char SpecialChars[256] = {0};
    char GeneratedString[131] = {0}; // Ubah ukuran array sesuai dengan MaximLen + 1
    const int Valid_Chars_len = strlen(ValidChars);

    SpecialChars[0] = ValidChars[0];

    for (int i = 0; i < Valid_Chars_len - 1; i++)
        SpecialChars[ValidChars[i]] = ValidChars[i + 1];

    memset(GeneratedString, ValidChars[0], MinimLen);

    char *Pointer = GeneratedString;
    char *PointerEnd = GeneratedString + MaximLen;
    char NextChar = 0;
    while (1)
    {
        Pointer = GeneratedString;
        extracted = extract_archive(GeneratedString, "archive/archive.zip", "archive/output");

        if (extracted)
        {
            printf("try = [%s]\t: PASS\n", GeneratedString);
            exit(0);
        }
        printf("try = [%s]\t: FAILED\n", GeneratedString);

        while (1)
        {
            NextChar = SpecialChars[*Pointer];
            if (NextChar != 0)
            {
                *Pointer = NextChar;
                break;
            }
            else
            {
                *Pointer = SpecialChars[0];
                Pointer++;

                if (Pointer >= PointerEnd)
                    return 0;
            }
        }
    }

    return 0;
}
