#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <sys/wait.h>
#include <sys/mman.h>

#define maxChild 5

bool extract_archive(const char *password, const char *input, const char *output)
{
    int size = snprintf(NULL, 0, "7z x -p'%s' -o%s -aoa %s 1>NUL 2>NUL", password, output, input);

    char *command = (char *)malloc(size + 1);

    if (command == NULL)
    {
        return false;
    }

    snprintf(command, size + 1, "7z x -p'%s' -o%s -aoa %s 1>NUL 2>NUL", password, output, input);
    int result = system(command);
    free(command);

    return result == 0;
}

void copy_file(const char source[], const char target[])
{
    char command[256];
    snprintf(command, sizeof(command), "cp %s %s", source, target);
    system(command);
}

void remove_file(const char file[])
{
    char command[256];
    snprintf(command, sizeof(command), "rm %s", file);
    system(command);
}

int or_true(bool tester[])
{
    for (size_t i = 0; i < maxChild; i++)
    {
        if (tester[i])
            return true;
    }
    return false;
}

int main()
{
    bool *extracted = mmap(NULL, maxChild * sizeof(bool),
                           PROT_READ | PROT_WRITE,
                           MAP_SHARED | MAP_ANONYMOUS,
                           0, 0);
    char *finalPass = mmap(NULL, maxChild * sizeof(char),
                           PROT_READ | PROT_WRITE,
                           MAP_SHARED | MAP_ANONYMOUS,
                           0, 0);

    for (size_t setter = 0; setter < maxChild; setter++)
    {
        extracted[setter] = false;
    }

    for (size_t childNum = 1; childNum <= maxChild; childNum++)
    {
        if (or_true(extracted))
            exit(0);
        if (fork() == 0)
        {

            char filename[256];
            snprintf(filename, sizeof(filename), "archive/%d.zip", (int)childNum);
            copy_file("archive/archive.zip", filename);
            if (or_true(extracted))
            {
                remove_file(filename);

                exit(0);
            }
            const char *ValidChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()_+-={}[]|\\:;\"'<>,.?/~` ";
            const int MinimLen = childNum;
            const int MaximLen = childNum;
            char SpecialChars[256] = {0};
            char GeneratedString[256] = {0};
            const int Valid_Chars_len = strlen(ValidChars);

            SpecialChars[0] = ValidChars[0];

            for (int i = 0; i < Valid_Chars_len - 1; i++)
                SpecialChars[ValidChars[i]] = ValidChars[i + 1];

            memset(GeneratedString, ValidChars[0], MinimLen);

            char *Pointer = GeneratedString;
            char *PointerEnd = GeneratedString + MaximLen;
            char NextChar = 0;

            while (!or_true(extracted))
            {
                Pointer = GeneratedString;

                extracted[childNum - 1] = extract_archive(GeneratedString, filename, "archive/extracted");
                if (extracted[childNum - 1])
                {
                    strcpy(finalPass, GeneratedString);
                    goto EXIT;
                }

                if (or_true(extracted))
                {
                    printf("try = [%s]\t: PASS\n", GeneratedString);
                    goto EXIT;
                }
                printf("try = [%s]\t: FAILED\n", GeneratedString);

                // printf("child %d, status %d\n", childNum, or_true(extracted));

                while (!or_true(extracted))
                {
                    NextChar = SpecialChars[*Pointer];
                    if (NextChar != 0)
                    {
                        *Pointer = NextChar;
                        break;
                    }
                    else
                    {
                        *Pointer = SpecialChars[0];
                        Pointer++;

                        if (Pointer >= PointerEnd)
                            goto EXIT;
                    }
                }
            }
        EXIT:
            remove_file(filename);
            exit(0);
        }
    }

    for (int i = 1; i < 130; i++)
        wait(NULL);

    printf("ARCHIVE PASSWORD = [%s]\n", finalPass);
}
